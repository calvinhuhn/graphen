import java.util.LinkedList;
import java.util.ListIterator;
import java.util.PriorityQueue;
//IVs

LinkedList knoten = new LinkedList();
//einseitige Adjazenzmatrix
LinkedList kanten = new LinkedList();


char PressedKeyCode;
//color presets

color white = color(255, 255, 255);
color gray = color(230, 230, 230);
color dark_gray = color(155, 155, 155);
color red = color(255, 0, 0);

//hilfsvariablen
boolean rel = true;
boolean dijkstraAktiviert = false;
Knoten angewaehlterKnoten;

//Grafik-Presets
public final int groesseKnoten = 60;
public final int border = 3;

void setup() {
  size(840, 1030);
  surface.setResizable(true);
  surface.setTitle("Graphenaufgabe");
  System.out.println("Anwendung wurde gestartet");
}

void draw() {
  grafikErzeugen();
}

void keyPressed() {
  switch(key) {
  case 'a':
  case 'A': 
    erzeugeKnoten(mouseX, mouseY); 
    dijkstraAktiviert = true;
    wegFinden();
    break; 
    /*--------- reset -----------*/
  case 'r':
  case 'R': 
    reset();  
    break; 
    /*--------- graphen erzeugen -----------*/
  case '!': 
    generateSampleGraph(1); 
    break;  
  case '"': 
    generateSampleGraph(2);  
    break; 
  case '§': 
    generateSampleGraph(3);  
    break;  
  case '$': 
    generateSampleGraph(4);  
    break;  
  case '%': 
    generateSampleGraph(5);  
    break;  
  case '&': 
    generateSampleGraph(6);  
    break; 
  case '/': 
    generateSampleGraph(7);  
    break;  
  case '(': 
    generateSampleGraph(8);  
    break;   
  case ')': 
    generateSampleGraph(9);  
    break;  
  case 'd': 
    /*--------- dijkstra Aktivieren oder deaktivieren -----------*/
    wegFinden();  
    break;
  }
}



void generateSampleGraph(int anzahl) {
  //graf wird vorher deaktiviert
  reset();
  for (int i = anzahl; i > 0; i --) {
    //Knoten mit zufälliger position wird beliebig oft erzeugt
    erzeugeKnoten(int(random(0 + 80, width - 80)), int(random(0 + 80, height - 80)));
  } 
  ListIterator<Knoten> iteratorA = knoten.listIterator(0);  
  while (iteratorA.hasNext()) {
    Knoten knotenA = iteratorA.next();

    ListIterator<Knoten> iteratorB = knoten.listIterator(0);  
    while (iteratorB.hasNext()) {
      Knoten knotenB = iteratorB.next();
      int zufallswert = int(random(0, 7));

      if (knotenA!=knotenB && zufallswert == 0) {
        setzeKante(knotenA, knotenB, int(random(1, 10)));
      }
    }
  }
}


void mouseDragged() {
  if (angewaehlterKnoten!=null && !keyPressed) {
    angewaehlterKnoten.updatePosition();
  }
}



public boolean aufPositionIstKnoten(int x, int y) {
  ListIterator<Knoten> iterator = knoten.listIterator(0);  
  while (iterator.hasNext()) {
    Knoten aktuellerKnoten = iterator.next();
    //Iteriere durch jeden Knoten und prüfe mit Knoten eigener Methode ob dier koordinarten mit einem bestimmten differenzbereich übereinstimmen
    if (aktuellerKnoten.istInHitbox(x, y)) {
      return true;
    }
  }
  return false;
}

void mousePressed() {
  angewaehlt();

  if (keyPressed && keyCode==SHIFT && !aufPositionIstKnoten(mouseX, mouseY)) {
    erzeugeKnoten(mouseX, mouseY);
    //neuen knoten erzeugen an position
  } else if (keyPressed && keyCode == SHIFT && angewaehlterKnoten!= null && aufPositionIstKnoten(mouseX, mouseY)) {
    if (angewaehlterKnoten!=null && aufPositionIstKnoten(mouseX, mouseY)) {
      //erzeugt die kannte
      setzeKante(angewaehlterKnoten, getKnotenAnPos(mouseX, mouseY), int(random(1, 6)));
    }
  } else {
    //entmarkiert und markjiert knoten 
    ListIterator<Knoten> iterator = knoten.listIterator(0);
    boolean knotenGefunden = false;

    while (iterator.hasNext()) {
      Knoten aktuellerKnoten = iterator.next();
      if (aktuellerKnoten.istInHitbox(mouseX, mouseY)) {
        angewaehlterKnoten = aktuellerKnoten;
        knotenGefunden = true;
      }
    }
    if (!knotenGefunden) {   
      angewaehlterKnoten = null;
    }
  }
}
void mouseMoved() {
  //bei jedem move von der maus wird die hover methode ausgeführt 
  hover();
}

public Knoten getKnotenAnPos(int x, int y) {
  ListIterator<Knoten> iterator = knoten.listIterator(0);
  while (iterator.hasNext()) {
    Knoten aktuellerKnoten = iterator.next();
    if (aktuellerKnoten.istInHitbox(x, y)) {
      return aktuellerKnoten;
    }
  }
  return null;
}

public void erzeugeKnoten(int x, int y) {
  knoten.add(new Knoten(y, x));
}



//}
public void setzeKante(Knoten a, Knoten b, int gewichtung) {
  // if(angewaehlterKnoten!= null) {
  boolean kantenIdentisch = false;
  ListIterator<Kante> iterator = kanten.listIterator(0);
  Kante neu = new Kante(a, b, gewichtung);

  while (iterator.hasNext()) {
    Kante aktuellerKante = iterator.next();
    if (aktuellerKante.isEqualTo(neu)) {
      kantenIdentisch = true;
    }
  }
  if (!kantenIdentisch) {
    kanten.add(neu);
    kantenIdentisch = false;
  }
}   

public void reset() {
  angewaehlterKnoten = null; 
  knoten = new LinkedList();
  kanten = new LinkedList();
}

/* 
 ALGORITHMUS # von Dijkstra
 Übergabedaten: Graph, startKnoten, zielKnoten
 # Vorbereitung des Graphen
 für alle knoten des Graphen:
 setze abstand auf 'u'
 setze herkunft auf None
 setze abstand von startKnoten.abstand auf 0
 füge startKnoten in eine Liste zuVerarbeiten ein
 # Verarbeitung der Knoten
 SOLANGE die Liste zuVerarbeiten nicht leer ist:
 
 bestimme einen Knoten minKnoten aus zuVerarbeiten mit minimalem Abstand
 entferne minKnoten aus zuVerarbeiten
 für alle nachbarKnoten von minKnoten:
 gewicht = Gewicht der Kante von minKnoten zu nachbarKnoten
 neuerAbstand = (abstand von minKnoten) + gewicht
 WENN abstand von nachbarKnoten == 'u':
 füge nachbarKnoten in die Liste zuVerarbeiten ein (z.B. am Listenende)
 setze abstand von nachbarKnoten auf neuerAbstand
 setze herkunft von nachbarKnoten auf minKnoten
 SONST:
 WENN nachbarKnoten in zuVerarbeiten liegt:
 WENN abstand von nachbarKnoten > neuerAbstand:
 setze abstand von nachbarKnoten auf neuerAbstand
 setze herkunft von nachbarKnoten auf minKnoten
 weglaenge = abstand von zielKnoten
 # Erzeugung des Weges zum zielKnoten
 weg = []
 knoten = zielKnoten    
 SOLANGE knoten != startKnoten und herkunft von knoten != None:
 weg = [(herkunft von knoten, knoten)] + weg
 knoten = herkunft von knoten
 Rückgabedaten: (weg, weglaenge)
 */

public void dijkstra(Knoten startKnoten, Knoten Zielknoten) {
  PriorityQueue zuverarbeiten = new PriorityQueue();
  //# Vorbereitung des Graphen
  //für alle knoten des Graphen:
  // setze abstand auf 'u'
  //setze herkunft auf None
  //setze abstand von startKnoten.abstand auf 0
  //füge startKnoten in eine Liste zuVerarbeiten ein

  ListIterator<Knoten> iterator = knoten.listIterator(0);  
  while (iterator.hasNext()) {
    Knoten aktuellerknoten = iterator.next();

    aktuellerknoten.initialisiereAbstand();
    aktuellerknoten.setzeHerkunft(null);
    aktuellerknoten.inRoute = false;
    zuverarbeiten.add(startKnoten);
  }
  startKnoten.setzeAbstand(0f);

  while (!zuverarbeiten.isEmpty()) {

    //bestimme einen Knoten minKnoten aus zuVerarbeiten mit minimalem Abstand
    Knoten minKnoten = (Knoten)zuverarbeiten.poll();
    //für alle nachbarKnoten von minKnoten:
    ListIterator<Kante> kantenIterator = kanten.listIterator();  
    while (kantenIterator.hasNext()) {

      Kante aktuelleKante = kantenIterator.next();
      Knoten updateKnoten = null;
      boolean gefunden = false;
      //suche den richtigen Knoten raus
      if (minKnoten == aktuelleKante.knotenA) {
        updateKnoten = aktuelleKante.knotenB;
        gefunden = true;
      } else if (minKnoten == aktuelleKante.knotenB) {
        updateKnoten = aktuelleKante.knotenA;
        gefunden = true;
      } 
      if (gefunden) {
        //gewicht = Gewicht der Kante von minKnoten zu nachbarKnoten
        //neuerAbstand = (abstand von minKnoten) + gewicht
        int gewicht = aktuelleKante.gewichtung;
        float neuerAbstand = minKnoten.abstand + gewicht;
        if (updateKnoten.abstand >= Float.POSITIVE_INFINITY) {
          //füge nachbarKnoten in die Liste zuVerarbeiten ein (z.B. am Listenende)
          //setze abstand von nachbarKnoten auf neuerAbstand
          //setze herkunft von nachbarKnoten auf minKnoten
          zuverarbeiten.add(updateKnoten);
          updateKnoten.abstand = neuerAbstand;
          updateKnoten.herkunft = minKnoten;
        } else {
          if (zuverarbeiten.contains(updateKnoten)) {
            if (updateKnoten.abstand > neuerAbstand) {
              updateKnoten.abstand = neuerAbstand;
              updateKnoten.herkunft = minKnoten;
            }
          }
          //WENN nachbarKnoten in zuVerarbeiten liegt:
          // WENN abstand von nachbarKnoten > neuerAbstand:
          //setze abstand von nachbarKnoten auf neuerAbstand
          //setze herkunft von nachbarKnoten auf minKnoten
        }
      }
    }
  }

  //knoten = zielKnoten    
  //SOLANGE knoten != startKnoten und herkunft von knoten != None:
  // weg = [(herkunft von knoten, knoten)] + weg
  //knoten = herkunft von knoten
  //Rückgabedaten: (weg, weglaenge)
  Knoten knoten = Zielknoten;
  while (knoten.herkunft != null) {
    knoten.inRoute = true;
    updateWegKanten(knoten, knoten.herkunft);
    knoten = knoten.herkunft;
  }
  knoten.inRoute = true;
  //updateWegKanten(knoten, knoten.herkunft);
}
public void wegFinden() {
  if (dijkstraAktiviert) {
    dijkstraAktiviert = false;
    resetWegKanten();
    resetWegKoten();
  } else {
    Knoten zielKnoten = getKnotenAnPos(mouseX, mouseY);
    resetWegKanten();
    if (angewaehlterKnoten!= null && zielKnoten != null) {
      dijkstraAktiviert = true;
      dijkstra(angewaehlterKnoten, zielKnoten);
    }
  }
}



public void grafikErzeugen() {
  //grafik wird zurückgesetzt
  background(white);

  /*---------- Kanten zeichnen ----------*/

  ListIterator<Kante> KantenIterator = kanten.listIterator(0);  
  while (KantenIterator.hasNext()) {
    Kante aktuelleKante = KantenIterator.next();
    //Zeichnet die Kanten die sich in einer Route befinden
    if (aktuelleKante.inRoute) {
      grafikKanten(aktuelleKante, red, red, white);
    } else if (aktuelleKante.angewaehlt && aktuelleKante.hover) {
      //Zeichnet die Kanten, die ausgewählt und gehovert werden
      grafikKanten(aktuelleKante, dark_gray, dark_gray, white);
    } else if (aktuelleKante.angewaehlt) {
      //zeichnet die Kanten die nur angewählt sind
      grafikKanten(aktuelleKante, dark_gray, white, dark_gray);
    } else if (aktuelleKante.hover) {
      //zeichnet die Kanten die gehovert werden 
      grafikKanten(aktuelleKante, dark_gray, dark_gray, white);
    } else {
      grafikKanten(aktuelleKante, gray, gray, white);
    }
  }

  //jeden Knoten durchlaufen und entsprechend nach eigenschaften zeichnen

  /*---------- Knoten zeichnen ----------*/
  ListIterator<Knoten> iterator = knoten.listIterator(0);  
  while (iterator.hasNext()) {
    Knoten aktuellerKnoten = iterator.next();
    if (aktuellerKnoten.inRoute) {
      grafikKnoten(aktuellerKnoten, red, red, white);
    } else if (aktuellerKnoten.angewaehlt && aktuellerKnoten.hover) {
      grafikKnoten(aktuellerKnoten, dark_gray, dark_gray, white);
    } else if (aktuellerKnoten.angewaehlt) {
      grafikKnoten(aktuellerKnoten, gray, dark_gray, white);
    } else if (aktuellerKnoten.hover) {
      grafikKnoten(aktuellerKnoten, dark_gray, white, dark_gray);
    } else {
      grafikKnoten(aktuellerKnoten, gray, white, dark_gray);
    }
  }
}

public void grafikKnoten(Knoten aktuellerKnoten, color randfarbe, color hintergrundfarbe, color textfarbe) {
  //speichert die Position von dem Knoten 
  noStroke();
  PVector aktuellerKnotenPosition = aktuellerKnoten.getPosition();
  //Hintergrundfarbe für den Hintergrund
  fill(randfarbe);
  //füllt den mittelkreis (etwas kleiner als der umrandungskreis mit der akzentfarbe. 
  ellipse(aktuellerKnotenPosition.x, aktuellerKnotenPosition.y, groesseKnoten, groesseKnoten);
  fill(hintergrundfarbe);
  ellipse(aktuellerKnotenPosition.x, aktuellerKnotenPosition.y, groesseKnoten - border, groesseKnoten - border);
  textSize(32);
  textAlign(CENTER, CENTER);
  fill(textfarbe);
  int textNummer;
  if (aktuellerKnoten.knotennummer == 0) {
    textNummer = knoten.indexOf(aktuellerKnoten);
  } else {
    textNummer = aktuellerKnoten.knotennummer;
  }
  text("" + textNummer, aktuellerKnotenPosition.x, aktuellerKnotenPosition.y - 3);
}

public void grafikKanten(Kante aktuelleKante, color linienfarbe, color hintergrundfarbe, color textfarbe) {
  //linie zeichnen
  fill(hintergrundfarbe);
  stroke(linienfarbe);  
  line(aktuelleKante.ersterKnoten().getPosition().x, aktuelleKante.ersterKnoten().getPosition().y, 
    aktuelleKante.zweiterKnoten().getPosition().x, aktuelleKante.zweiterKnoten().getPosition().y);

  //Textposition  
  PVector positionText;
  fill(hintergrundfarbe);
  positionText = aktuelleKante.mittelpunkt();
  textSize(groesseKnoten / 3);
  fill(hintergrundfarbe);
  ellipse(positionText.x, positionText.y, groesseKnoten / 2, groesseKnoten /2);
  fill(textfarbe);
  text(aktuelleKante.getGewicht(), positionText.x, positionText.y -1);
}

public void updateWegKanten(Knoten a, Knoten b) {
  //durchsucht die ganze Liste um die kante zu finden, in dem der herkunftsknoten und der eigentliche knoten ist um sie als "in route" kennzeichnen zu können
  ListIterator<Kante> KantenIterator = kanten.listIterator(0);  
  while (KantenIterator.hasNext()) {
    Kante aktuelleKante = KantenIterator.next();
    if ((aktuelleKante.knotenA == a && aktuelleKante.knotenB == b) || (aktuelleKante.knotenA == b && aktuelleKante.knotenB == a)) {
      aktuelleKante.inRoute = true;
    }
  } //Ende While
} //Ende updateWegKanten

public void resetWegKanten() {
  //setzt den zustand von allen Kanten die zum schnellsten Weg gehören zurück... 
  ListIterator<Kante> KantenIterator = kanten.listIterator(0);  
  while (KantenIterator.hasNext()) {
    Kante aktuelleKante = KantenIterator.next();
    aktuelleKante.inRoute = false;
  }//Ende Iterator Kanten
}//Ende Klasse resetWegKanten

public void resetWegKoten() {
  //setzt den zustand von allen Kanten die zum schnellsten Weg gehören zurück... 
  ListIterator<Knoten> KnotenIterator = knoten.listIterator(0);  
  while (KnotenIterator.hasNext()) {
    Knoten aktuellerKnoten = KnotenIterator.next();
    aktuellerKnoten.inRoute = false;
  }//Ende Iterator Kanten
}//Ende Klasse resetWegKanten

public void resetHover() {
  //ändert den hover status von jeder kante und setzt sie auf false
  ListIterator<Kante> KantenIterator = kanten.listIterator(0);  
  while (KantenIterator.hasNext()) {
    Kante aktuelleKante = KantenIterator.next();
    aktuelleKante.hover = false;
  } //Ende Iterator Kante
  //ändert den hover status von jedem Knoten und setzt sie auf false
  ListIterator<Knoten> KnotenIterator = knoten.listIterator(0);  
  while (KnotenIterator.hasNext()) {
    Knoten aktuellerKnoten = KnotenIterator.next();
    aktuellerKnoten.hover = false;
  } //Ende Iterator Knoten
}//Ende resetHover 


public void hover() {
  Knoten knotenAnPosition = getKnotenAnPos(mouseX, mouseY);
  resetHover();
  if (knotenAnPosition!= null) {
    knotenAnPosition.hover = true;
    ListIterator<Kante> iteratorKanten = kanten.listIterator(0);
    while (iteratorKanten.hasNext()) {
      Kante aktuelleKante = iteratorKanten.next();
      if (aktuelleKante.knotenA == knotenAnPosition || aktuelleKante.knotenB == knotenAnPosition ) {
        aktuelleKante.hover = true;
      }//Ende if Knoten in Kante
    }//EndeIterator
  }
}//Ende Hover

public void angewaehlt() {
  Knoten knotenAnPosition = getKnotenAnPos(mouseX, mouseY);
  resetAngewaehlt();
  if (knotenAnPosition!= null) {
    knotenAnPosition.angewaehlt = true;
    ListIterator<Kante> iteratorKanten = kanten.listIterator(0);
    while (iteratorKanten.hasNext()) {
      Kante aktuelleKante = iteratorKanten.next();
      if (aktuelleKante.knotenA == knotenAnPosition || aktuelleKante.knotenB == knotenAnPosition ) {
        aktuelleKante.angewaehlt = true;
      }//Ende if Knoten in Kante
    }//EndeIterator
  }
}

public void resetAngewaehlt() {
  //ändert den hover status von jeder kante und setzt sie auf false
  ListIterator<Kante> KantenIterator = kanten.listIterator(0);  
  while (KantenIterator.hasNext()) {
    Kante aktuelleKante = KantenIterator.next();
    aktuelleKante.angewaehlt = false;
  } //Ende Iterator Kante
  //ändert den hover status von jedem Knoten und setzt sie auf false
  ListIterator<Knoten> KnotenIterator = knoten.listIterator(0);  
  while (KnotenIterator.hasNext()) {
    Knoten aktuellerKnoten = KnotenIterator.next();
    aktuellerKnoten.angewaehlt = false;
  } //Ende Iterator Knoten
}//Ende resetHover 
