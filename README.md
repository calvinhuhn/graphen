# IP 2 Graphs with Dijkstra's algorithm
Whith this programm you are able to generate graphs. 
Dijkstra's algorithm is an algorithm for finding the shortest paths between nodes in a graph, which may represent, for example, road networks. 


## Usage
To create a random undirected graph simply press `shift` and ` a number bewteen 1 and 9` representing a Degree. 
To add new Vertex press the key `a` and press the mouse on the desired position. 
If you want to add a new edge bewteen to vertices, select the first vertex, press `shift` and select the second vertex.
By selecting the Vertex and moving the mouse, you are able to change each nodes position.
Deleting your graph is as simple as pressing `r`. 
To start Dijkstra's algorithm, select the start node and select the second node while pressing `d` afterwards. The fasted path sould be marked red. 

## Prerequisites
You need a working JDK and Processing installation



