public class Kante{
    Knoten knotenA;
    Knoten knotenB;
    int gewichtung;
    
    boolean angewaehlt = false;
    boolean hover = false;
    boolean inRoute = false;
    
    public Kante(Knoten _a, Knoten _b, int _schwere) {
      knotenA = _a;
      knotenB = _b;
      setzeGewichtung(_schwere);
      //System.out.println("Kante wurde erzeugt");
    }
    
    
    public int getGewicht() {
      return gewichtung;
    }
    
    public void setzeGewichtung(int _gewichtung) {
      gewichtung = _gewichtung;
    }
    
    public boolean isEqualTo(Kante neu){
      if((neu.knotenA == this.knotenA && 
         neu.knotenB == this.knotenB) || 
         (neu.knotenB == this.knotenA && 
         neu.knotenA == this.knotenB)) {
        return true;
      } else {
        return false;
      }
    }
    
    public boolean beinhaltetKnoten(Knoten a) {
    return (a == knotenA || a == knotenB);
    }
    
    public Knoten ersterKnoten() {
      return knotenA;
    }
    
    public Knoten zweiterKnoten() {
      return knotenB;
    }
    public PVector mittelpunkt() {
      PVector mittelpunkt = (knotenA.getPosition().copy().add(knotenB.getPosition())).div(2);
      return mittelpunkt;
    }
    
    
}
