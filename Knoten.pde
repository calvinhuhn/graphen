public class Knoten implements Comparable<Knoten>{
//IV's
PVector position;
Float abstand;
Knoten herkunft;
boolean angewaehlt = false;
boolean hover = false;
boolean inRoute = false;
int knotennummer;

public Knoten() {
  initialisiereAbstand();

}

public Knoten(int _x, int _y) {
  position = new PVector(_y, _x);
  initialisiereAbstand();
}

public PVector getPosition() {
  return position;
}

public String toString() {
    return "Der Knoten hat die Position X: " + position.x + " , " + position.y;
}

public Boolean istInHitbox(int x, int y) {
    return ((x > (position.x - groesseKnoten+20) 
            && x < (position.x + groesseKnoten-20)) 
            && 
            (y > (position.y - groesseKnoten+20) 
            && y < (position.y + groesseKnoten-20))) ;
    }
    
    public void updatePosition() {
  position.x = mouseX;
  position.y = mouseY;
  
}

public void initialisiereAbstand() {
  abstand = Float.POSITIVE_INFINITY;
}

public int compareTo(Knoten other) {
  return (int)(this.abstand - other.abstand);
}

public void setzeAbstand(Float a) {
  abstand = a;
}
public void setzeHerkunft(Knoten _knoten) {
  herkunft = _knoten;
}


}
